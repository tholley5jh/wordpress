<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'YX>6)nMM)texmDNQ%h0NZaDrj]szg]g{=:qHh YP]79o]Z^@`xu:~tTorK71N)n@' );
define( 'SECURE_AUTH_KEY',  'TzFqJpTX#-uOuc^/p/e1%z0MH8&d|^(e[k>o3zKC}o9xT2e#FKp#wn4 Tbt@mLcj' );
define( 'LOGGED_IN_KEY',    'yc70f9N.Q8uio4,c{u9hXBUnv:Q~Q.lwy/oL[&_a=C% ~pyZLGq*JmJD:W?W2xw*' );
define( 'NONCE_KEY',        'elsLP>ZW zu9TmAwrc!vb?h^XRz63yzl9BRVx^7-S8FgU;Kd :T]+MG25f) X^Os' );
define( 'AUTH_SALT',        'mOJtr`Ra$4px;IY z~x_1dZM%n>SSx6:sH6V-:duGbs07A,zbyz&H2LOo}fc54#(' );
define( 'SECURE_AUTH_SALT', 'l$)h_|$T+:mb8Bg%5wQZ>mwh-#e@U67[~Z[S.48{ksg5H`@*{l%[u7]aAVT:H{2{' );
define( 'LOGGED_IN_SALT',   'K,1`t:,b/*|U$e>K-<h5YJTcf%(G:H7>418,N<17o>=.Btqq^9K9-7qv)IC@%|YN' );
define( 'NONCE_SALT',       'j/Urxi} 0(^5 ,UUR.2;mvGm9TY|l;]M]v^&J9[@fDfTbs{-0>x,W[z8<dTO:YXC' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
